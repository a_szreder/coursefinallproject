package pl.as.casheer.spring.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.WorkingDateTime;

@Repository
public interface WorkingDateTimeRepository extends CrudRepository<WorkingDateTime, Long>{

}
