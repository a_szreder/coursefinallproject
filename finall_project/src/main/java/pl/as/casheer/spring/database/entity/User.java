package pl.as.casheer.spring.database.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity(name="no_user")
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name = "user_id")
	private long id;
	
	private String name;
	private String surname;
	
	@Column(unique=true)
	private String login;
	private String password;
	private LocalDate assumptionDate;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="user_permission", 
		joinColumns= {@JoinColumn(name = "user_id")},
		inverseJoinColumns= {@JoinColumn(name="permission_id")})
	private Set<Permission> permissions = new HashSet<>();
	
	@ManyToMany(mappedBy="users")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<SalePoint> salePoints = new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@LazyCollection(LazyCollectionOption.FALSE)
	Set<WorkingDateTime> workingTimes= new HashSet<>();
	
	public User() {
	}

	public User(String name, String surname, String login, String password) {
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.password = password;
		assumptionDate = LocalDate.now();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getAssumptionDate() {
		return assumptionDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAssumptionDate(LocalDate assumptionDate) {
		this.assumptionDate = assumptionDate;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Set<SalePoint> getSalePoints() {
		return salePoints;
	}

	public void setSalePoints(Set<SalePoint> salePoints) {
		this.salePoints = salePoints;
	}
	
	@PreRemove
	public void removeSalePointFromUser() {
		for(SalePoint salePoint : salePoints) {
			salePoint.getUsers().remove(this);
		}
	}

	public Set<WorkingDateTime> getWorkingTimes() {
		return workingTimes;
	}

	public void setWorkingTimes(Set<WorkingDateTime> workingTimes) {
		this.workingTimes = workingTimes;
	}
}
