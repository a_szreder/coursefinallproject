package pl.as.casheer.spring.database.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

	@Query("select u from no_user u where u.login =?1 and u.password=?2")
	public Optional<User> findByLoginAndPassword(String login, String password);
	
	@Query("select u from no_user u where u.login =?1")
	public Optional<User> findByLogin(String login);
}
