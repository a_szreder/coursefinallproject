package pl.as.casheer.spring.database.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.Permission;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long>{

	@Query("select perm from permission perm where perm.name =?1")
	public Optional<Permission> findByName(String name);
}
