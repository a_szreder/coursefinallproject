package pl.as.casheer.spring.database.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity(name="permission")
public class Permission implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name = "permission_id")
	private long id;
	private String name;
	
	@ManyToMany(mappedBy="permissions")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<User> users = new HashSet<>();
	
	public Permission() {
	}

	public Permission(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	@PreRemove
	public void removePermissionFromUsers() {
		for(User u : users) {
			u.getPermissions().remove(this);
		}
	}
}
