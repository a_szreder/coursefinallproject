package pl.as.casheer.spring.database.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.Permission;
import pl.as.casheer.spring.database.entity.ProductQuantity;

@Repository
public interface ProductQuantityRepository extends CrudRepository<ProductQuantity, Long>{

	@Query("select pq from ProductQuantity pq where pq.idOfStore =?1 and idOfProduct = ?2")
	public Optional<ProductQuantity> findByStoreIdAndProductId(long idOfStore, long idOfProduct);
}
