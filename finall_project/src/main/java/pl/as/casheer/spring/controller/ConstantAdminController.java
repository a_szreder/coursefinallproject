package pl.as.casheer.spring.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.as.casheer.spring.database.entity.Permission;
import pl.as.casheer.spring.database.entity.User;
import pl.as.casheer.spring.database.repository.PermissionRepository;
import pl.as.casheer.spring.database.repository.UserRepository;

@Controller
public class ConstantAdminController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PermissionRepository permissionRepository;

	@RequestMapping("/getAdmin")
	public String createAdmin() {
		Optional<Permission> adminPermission = permissionRepository.findByName("administrator");

		if (!adminPermission.isPresent()) {
			Permission administratorPermission = new Permission("administrator");
			permissionRepository.save(administratorPermission);
		}

		Optional<Permission> workerPermission = permissionRepository.findByName("worker");
		if (!workerPermission.isPresent()) {
			Permission worker = new Permission("worker");
			permissionRepository.save(worker);
		}

		Optional<User> administrator = userRepository.findByLoginAndPassword("admin", "admin");
		if (!administrator.isPresent()) {
			User user = new User("Admin", "Admin", "admin", "admin");
			userRepository.save(user);
		}

		Optional<User> adminPerm = userRepository.findByLogin("admin");
		Set<Permission> permissionAdmin = adminPerm.get().getPermissions();
		boolean isThereAdmin = false;
		for (Permission perm : permissionAdmin) {
			if ("administrator".equals(perm.getName())) {
				isThereAdmin = true;
				break;
			}
		}
		if (isThereAdmin == false) {
			User admin = userRepository.findByLoginAndPassword("admin", "admin").get();
			admin.getPermissions().add(permissionRepository.findByName("administrator").get());
			userRepository.save(admin);
		}

		return "index";
	}
}
