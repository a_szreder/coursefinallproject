package pl.as.casheer.spring.database.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Product implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name= "product_id")
	private long id;
	
	private String name;
	private double price;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="product")
	@LazyCollection(LazyCollectionOption.FALSE)
	Set<ProductQuantity> productQuantity = new HashSet<>();
	
	public Product() {
	}

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Set<ProductQuantity> getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Set<ProductQuantity> productQuantity) {
		this.productQuantity = productQuantity;
	}

	@PreRemove
	public void doNotRemoveStores() {
		
	}
}
