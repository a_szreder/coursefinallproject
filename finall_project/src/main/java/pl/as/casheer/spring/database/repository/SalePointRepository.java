package pl.as.casheer.spring.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.SalePoint;

@Repository
public interface SalePointRepository extends CrudRepository<SalePoint, Long>{

}
