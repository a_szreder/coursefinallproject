package pl.as.casheer.spring.database.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(ProductQuantityId.class)
public class ProductQuantity {

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "store_id", nullable = false)
	private Store store;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;

	@Id
	private long idOfStore;
	@Id
	private long idOfProduct;

	private long quantity;

	public ProductQuantity() {
	}

	public ProductQuantity(Store store, Product product, long quantity) {
		this.store = store;
		this.product = product;
		this.quantity = quantity;
		idOfStore = store.getId();
		idOfProduct = product.getId();
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public long getIdOfStore() {
		return idOfStore;
	}

	public void setIdOfStore(long idOfStore) {
		this.idOfStore = idOfStore;
	}

	public long getIdOfProduct() {
		return idOfProduct;
	}

	public void setIdOfProduct(long idOfProduct) {
		this.idOfProduct = idOfProduct;
	}


	
}
