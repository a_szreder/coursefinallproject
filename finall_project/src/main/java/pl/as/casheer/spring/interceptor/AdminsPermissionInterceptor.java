package pl.as.casheer.spring.interceptor;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pl.as.casheer.spring.database.entity.Permission;
import pl.as.casheer.spring.database.entity.User;

public class AdminsPermissionInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		User user = (User) request.getSession().getAttribute("loggedUser");
		if (user != null) {
			Set<Permission> permissions = user.getPermissions();
			if (permissions != null && !permissions.isEmpty()) {
				for (Permission perm : permissions) {
					if ("administrator".equals(perm.getName())) {
						return super.preHandle(request, response, handler);
					}
				}
			}
		}
		request.setAttribute("error", "You do not have permit to do that");
		request.getRequestDispatcher("/error/404").forward(request, response);

		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
}
