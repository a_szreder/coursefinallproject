package pl.as.casheer.spring.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.as.casheer.spring.database.entity.Product;
import pl.as.casheer.spring.database.entity.ProductQuantity;
import pl.as.casheer.spring.database.entity.Store;
import pl.as.casheer.spring.database.repository.ProductQuantityRepository;
import pl.as.casheer.spring.database.repository.ProductRepository;
import pl.as.casheer.spring.database.repository.StoreRepository;

@Controller
@RequestMapping("/store")
public class StoreController {

	@Autowired
	StoreRepository storeRepository;

	@Autowired
	ProductQuantityRepository productQuantityRepository;

	@Autowired
	ProductRepository productRepository;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String allStores(ModelMap model) {
		List<Store> allStores = (List<Store>) storeRepository.findAll();
		model.addAttribute("stores", allStores);
		return "store/all";
	}

	@RequestMapping(value = "/addOne/{id}", method = RequestMethod.GET)
	public String addOneProductForm(@PathVariable("id") long id, ModelMap model) {

		Store store = storeRepository.findOne(id);
		model.addAttribute("store", store);

		List<Product> products = (List<Product>) productRepository.findAll();
		model.addAttribute("products", products);

		return "store/addProduct";
	}
	
	@RequestMapping(value="/allProducts/{id}", method=RequestMethod.GET)
	public String showAllProducts(@PathVariable("id") long id, ModelMap model) {
		Store store = storeRepository.findOne(id);
		model.addAttribute("store", store);
		return "store/showProducts";
	}

	@RequestMapping(value = "/addOne/{id}", method = RequestMethod.POST)
	public String addOneProduct(@PathVariable("id") long id, 
								@RequestParam("product") long productId,
								@RequestParam("quantity") long quantity, ModelMap model) {

		Store store = storeRepository.findOne(id);
		Product product = productRepository.findOne(productId);
		ProductQuantity productQuantity;

		Optional<ProductQuantity> productQuan = productQuantityRepository.findByStoreIdAndProductId(id, productId);
		if (productQuan.isPresent()) {
			productQuantity = productQuan.get();

			store.getProductQuantity().remove(productQuantity);
			product.getProductQuantity().remove(productQuantity);

			long currentQuantity = productQuantity.getQuantity() + quantity;
//			System.out.println(productQuantity.getQuantity());
//			System.out.println(currentQuantity);
			productQuantity.setQuantity(currentQuantity);
//			System.out.println(productQuantity.getQuantity());

		} else {
			productQuantity = new ProductQuantity(store, product, quantity);
//			System.out.println("Ale tu mnie nie ma");
		}
//		System.out.println(productQuantity.getQuantity());

		store.getProductQuantity().add(productQuantity);
		product.getProductQuantity().add(productQuantity);
//		System.out.println(productQuantity.getQuantity());

		productQuantityRepository.save(productQuantity);

		storeRepository.save(store);
		productRepository.save(product);
		
		String msg ="Product has been added properly";
		model.addAttribute("information", msg);

		List<Store> allStores = (List<Store>) storeRepository.findAll();
		model.addAttribute("stores", allStores);
		return "store/all";
	}
	//
	// @RequestMapping(value="/addMany/{id}", method=RequestMethod.GET)
	// public String addManyProductForm(@PathVariable("id") long id,ModelMap model)
	// {
	//
	// return "store/addManyProducts";
	// }
	//
	// @RequestMapping(value="/addMany/{id}", method=RequestMethod.POST)
	// public String addManyProduct(@PathVariable("id") long id,
	// @RequestParam("product") long[] productsId, ModelMap model) {
	//
	//
	//
	// List<Store> allStores = (List<Store>) storeRepository.findAll();
	// model.addAttribute("stores", allStores);
	// return "store/all";
	// }
}
