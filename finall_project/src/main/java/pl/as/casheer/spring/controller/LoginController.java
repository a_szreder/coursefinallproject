package pl.as.casheer.spring.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.as.casheer.spring.database.entity.User;
import pl.as.casheer.spring.database.entity.WorkingDateTime;
import pl.as.casheer.spring.database.repository.UserRepository;

@Controller
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value="/in", method=RequestMethod.GET)
	public String logForm() {
		return "loginForm";
	}

	@RequestMapping(value="/in", method=RequestMethod.POST)
	public String logIn(@RequestParam("login") String login,
						@RequestParam("password") String password, 
						HttpServletRequest request, ModelMap model) {
		
		Optional<User> optionalUser = userRepository.findByLoginAndPassword(login, password);
		String msg="";
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			if(password.equals(user.getPassword()) && login.equals(user.getLogin())) {
				request.getSession().setAttribute("startTime", LocalTime.now());
				request.getSession().setAttribute("loggedIn", true);
				request.getSession().setAttribute("loggedUser", user);
				msg = login + " you are logIn";
				model.addAttribute("information", msg);
			} 
		} else {
			msg = "Login or password are not correct";
			model.addAttribute("error", msg);
		}
		return "index";
	}
	
	@RequestMapping(value="/out", method=RequestMethod.GET)
	public String logOut(HttpServletRequest request, ModelMap model) {
		request.getSession().setAttribute("loggedIn", false);
		
		LocalTime startTime = (LocalTime) request.getSession().getAttribute("startTime");
		LocalTime endTime = LocalTime.now();
		LocalDate date = LocalDate.now();
		User user = (User) request.getSession().getAttribute("loggedUser");
		
		WorkingDateTime workedTime = new WorkingDateTime(date, startTime, endTime);
		workedTime.setUser(user);
		user.getWorkingTimes().add(workedTime);
		userRepository.save(user);
		
		request.getSession().removeAttribute("loggedUser");
		request.getSession().removeAttribute("startTime");
		
		
		String msg = "You are logOut properly, today you worked: "+ workedTime.getWorkedTime().toMinutes() +" minutes";
		model.addAttribute("information", msg);
		return "index";
	}
}
