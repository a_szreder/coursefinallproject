package pl.as.casheer.spring.database.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Store implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@Column(name = "store_id")
	private long id;

	private String name;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "store")
	@LazyCollection(LazyCollectionOption.FALSE)
	Set<ProductQuantity> productQuantity = new HashSet<>();

	@OneToOne(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private SalePoint salePoint;

	public Store() {
	}

	public Store(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductQuantity> getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Set<ProductQuantity> productQuantity) {
		this.productQuantity = productQuantity;
	}

	public SalePoint getSalePoint() {
		return salePoint;
	}

	public void setSalePoint(SalePoint salePoint) {
		this.salePoint = salePoint;
	}

	@PreRemove
	public void doNotRemoveProducts() {
		
	}
}
