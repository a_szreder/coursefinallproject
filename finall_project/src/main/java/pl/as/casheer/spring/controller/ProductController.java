package pl.as.casheer.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.as.casheer.spring.database.entity.Product;
import pl.as.casheer.spring.database.repository.ProductRepository;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductRepository productRepository;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public String allProducts(ModelMap model) {
		List<Product> allProducts = (List<Product>) productRepository.findAll();
		model.addAttribute("products", allProducts);
		return "product/all";
	}
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String addForm() {
		return "product/addEdit";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addProduct(@RequestParam("name") String name,
							 @RequestParam("price") double price,ModelMap model) {
		Product product = new Product(name, price);
		productRepository.save(product);
		
		String msg ="Product added properly";
		model.addAttribute("success", msg);
		
		List<Product> allProducts = (List<Product>) productRepository.findAll();
		model.addAttribute("products", allProducts);
		return "product/all";
	}
	
	@RequestMapping(value="/edit/{id}",method=RequestMethod.GET)
	public String editForm(@PathVariable("id") long id, ModelMap model) {
		Product product = productRepository.findOne(id);
		model.addAttribute("product", product);
		
		return "product/addEdit";
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public String editProduct(@PathVariable("id") long id,
							  @RequestParam("price") double price,ModelMap model) {
		Product product = productRepository.findOne(id);
		product.setPrice(price);
		productRepository.save(product);
		
		String msg ="Product edited properly";
		model.addAttribute("information", msg);
		
		List<Product> allProducts = (List<Product>) productRepository.findAll();
		model.addAttribute("products", allProducts);
		return "product/all";
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String deleteProduct(@PathVariable("id") long id, ModelMap model) {
		
		productRepository.delete(id);
		
		String msg ="Product deleted properly";
		model.addAttribute("information", msg);
		
		List<Product> allProducts = (List<Product>) productRepository.findAll();
		model.addAttribute("products", allProducts);
		return "product/all";
	}
}
