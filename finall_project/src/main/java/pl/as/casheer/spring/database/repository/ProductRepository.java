package pl.as.casheer.spring.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.as.casheer.spring.database.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>{

}
