package spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import spring.database.entity.Permission;
import spring.database.entity.User;
import spring.database.repository.PermissionRepository;
import spring.database.repository.UserRepository;

@Controller
@RequestMapping("/permission")
public class PermissionController {
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Autowired
	UserRepository userRepository;

	@RequestMapping(value="/show/{id}",method=RequestMethod.GET)
	public String showOne(@PathVariable("id") long id, ModelMap model) {
		Permission permission = permissionRepository.findOne(id);
		model.addAttribute("permission", permission);
		return "permission/showOne";
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		return "permission/all";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public String addForm() {
		return "permission/addEdit";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addRole(@RequestParam("name") String name, ModelMap model) {
		Permission permission = new Permission(name);
		permissionRepository.save(permission);
		
		String msg="Permission added properly";
		model.addAttribute("success", msg);
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		return "permission/all";
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String editForm(@PathVariable("id") long id, ModelMap model) {
		Permission permission = permissionRepository.findOne(id);
		model.addAttribute("permission", permission);
		return "permission/addEdit";
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public String editRole(@PathVariable("id") long id,
						   @RequestParam("name") String name, ModelMap model) {
		Permission permission = permissionRepository.findOne(id);
		permission.setName(name);
		permissionRepository.save(permission);
		
		String msg="Permission edited properly";
		model.addAttribute("information", msg);
		
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		return "permission/all";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String delete(@PathVariable("id") long id, ModelMap model) {
		
		permissionRepository.delete(id);
		
		String msg="Permission edited properly";
		model.addAttribute("information", msg);
		
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		return "permission/all";
	}
	
	@RequestMapping(value="/addUser/{id}", method=RequestMethod.GET)
	public String addUserForm(@PathVariable("id") long id, ModelMap model) {
		
		Permission permission = permissionRepository.findOne(id);
		model.addAttribute("permission", permission);
		
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		
		return "permission/addUser";
	}
	
	@RequestMapping(value="/addUser/{id}", method = RequestMethod.POST)
	public String addPermissionToUser(@PathVariable("id") long id,
									  @RequestParam("users") long[] usersId,
									  ModelMap model) {
		Permission permission = permissionRepository.findOne(id);
		List<User> permissionUsers = permission.getUsers();
		for(User u : permissionUsers) {
			u.getPermissions().remove(permissionRepository.findOne(id));
		}
		permissionUsers.clear();
		
		for(long userId : usersId) {
			permissionUsers.add(userRepository.findOne(userId));
			userRepository.save(userRepository.findOne(userId));
		}
		
		String msg="Users added properly";
		model.addAttribute("information", msg);
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		return "permission/all";
	}
}
