package spring.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import spring.database.entity.Permission;
import spring.database.entity.User;
import spring.database.repository.PermissionRepository;
import spring.database.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@RequestMapping(value="/show/{id}", method=RequestMethod.GET)
	public String showOneUser(@PathVariable("id") long id, ModelMap model) {
		User user = userRepository.findOne(id);
		model.addAttribute("user", user);
		return "user/showOne";
	}
	
	@RequestMapping(value="/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "user/all";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public String addForm() {
		return "user/addEdit";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addUser(@RequestParam("name") String name,
						  @RequestParam("surname") String surname,
						  @RequestParam("login") String login,
						  @RequestParam("passwordOne") String passwordOne,
						  @RequestParam("passwordTwo") String passwordTwo, ModelMap model) {
		
		String msg ="";
		
		Optional<User> isThereThisLogin = userRepository.findByLogin(login);
		if(!isThereThisLogin.isPresent()) {
		
			if(passwordOne.equals(passwordTwo)) {
				User user = new User(name, surname, login, passwordOne);
				userRepository.save(user);
				msg="User added properly";
				model.addAttribute("success", msg);
			}else {
				msg = "Passwords doesn't match";
				model.addAttribute("error", msg);
			}
		}else {
			msg="This login is already exists!";
			model.addAttribute("error", msg);
			return "user/addEdit";
		}
		
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "user/all";
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String editForm(@PathVariable("id") long id, ModelMap model) {
		User user = userRepository.findOne(id);
		model.addAttribute("user", user);
		return "user/addEdit";
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public String editUser(@PathVariable("id") long id,
						   @RequestParam("name") String name,
						   @RequestParam("surname") String surname,
						   @RequestParam("login") String login, ModelMap model) {
		
		User user = userRepository.findOne(id);
		user.setLogin(login);
		user.setName(name);
		user.setSurname(surname);
		userRepository.save(user);
		
		String msg="User edited properly";
		model.addAttribute("information", msg);
		
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "user/all";
	}
	
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String delete(@PathVariable("id") long id, ModelMap model) {
		
		User user = userRepository.findOne(id);
		List<Permission> permissions = user.getPermissions();
		for(Permission perm : permissions) {
			perm.getUsers().remove(user);
		}
		permissions.clear();
		
		userRepository.delete(user);
		
		String msg="User deleted properly";
		model.addAttribute("information", msg);
		
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "user/all";
	}
	
	@RequestMapping(value="/addPermission/{id}", method = RequestMethod.GET)
	public String addPermissionForm(@PathVariable("id") long id, ModelMap model) {
		User user = userRepository.findOne(id);
		model.addAttribute("user", user);
		
		List<Permission> listOfPermission = (List<Permission>) permissionRepository.findAll();
		model.addAttribute("permissions", listOfPermission);
		
		return "user/addPermission";
	}
	
	@RequestMapping(value="/addPermission/{id}", method = RequestMethod.POST)
	public String addPermissionToUser(@PathVariable("id") long id,
									  @RequestParam("permissions") long[] permissionsId,
									  ModelMap model) {
		User user = userRepository.findOne(id);
		List<Permission> userPermissions = user.getPermissions();
		for(Permission perm : userPermissions) {
			perm.getUsers().remove(userRepository.findOne(id));
		}
		userPermissions.clear();
		
		for(long permId : permissionsId) {
			userPermissions.add(permissionRepository.findOne(permId));
		}
		
		userRepository.save(user);
		
		String msg="Permissions added properly";
		model.addAttribute("information", msg);
		List<User> listOfUsers = (List<User>) userRepository.findAll();
		model.addAttribute("users", listOfUsers);
		return "user/all";
	}
}
