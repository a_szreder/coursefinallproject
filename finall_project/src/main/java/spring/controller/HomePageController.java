package spring.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import spring.database.entity.Permission;
import spring.database.entity.User;
import spring.database.repository.PermissionRepository;
import spring.database.repository.UserRepository;

@Controller

public class HomePageController {

	@RequestMapping("/")
	public String homePage() {

		return "index";
	}
}
