package spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import spring.database.entity.Permission;
import spring.database.entity.SalePoint;
import spring.database.entity.User;
import spring.database.repository.SalePointRepository;
import spring.database.repository.UserRepository;

@Controller
@RequestMapping("/salePoint")
public class SalePointController {

	@Autowired
	SalePointRepository salePointRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<SalePoint> listOfSalePoints = (List<SalePoint>) salePointRepository.findAll();
		model.addAttribute("salePoints", listOfSalePoints);
		return "salePoint/all";
	}
	
	@RequestMapping(value="/show/{id}", method=RequestMethod.GET)
	public String showOne(@PathVariable("id") long id, ModelMap model) {
		SalePoint salePoint = salePointRepository.findOne(id);
		model.addAttribute("salePoint", salePoint);
		return "salePoint/showOne";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.GET)
	public String addForm() {
		return "salePoint/addEdit";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addSalePoint(@RequestParam("name") String name,
							   @RequestParam("city") String city,
							   @RequestParam("street") String street,
							   @RequestParam("numberOfBuilding") int numberOfBuilding,
							   @RequestParam("apartmentNumber") int apartmentNumber,
							   @RequestParam("zipCode") String zipCode,
							   ModelMap model) {
		
		SalePoint salePoint = new SalePoint(name, city, zipCode, street, numberOfBuilding, apartmentNumber);
		salePointRepository.save(salePoint);
		
		String msg="Point of sale added properly";
		model.addAttribute("success", msg);
		
		List<SalePoint> listOfSalePoints = (List<SalePoint>) salePointRepository.findAll();
		model.addAttribute("salePoints", listOfSalePoints);
		return "salePoint/all";
	}
	
	@RequestMapping(value="/edit/{id}", method= RequestMethod.GET)
	public String editForm(@PathVariable("id") long id, ModelMap model) {
		SalePoint salePoint = salePointRepository.findOne(id);
		model.addAttribute("salePoint", salePoint);
		return "salePoint/addEdit";
	}
	
	@RequestMapping(value="/edit/{id}", method= RequestMethod.POST)
	public String editSalePoint(@PathVariable("id") long id, 
								@RequestParam("name") String name,
								@RequestParam("city") String city,
								@RequestParam("street") String street,
								@RequestParam("numberOfBuilding") int numberOfBuilding,
								@RequestParam("apartmentNumber") int apartmentNumber,
								@RequestParam("zipCode") String zipCode,
								ModelMap model) {
		SalePoint salePoint = salePointRepository.findOne(id);
		salePoint.setCity(city);
		salePoint.setApartmentNumber(apartmentNumber);
		salePoint.setName(name);
		salePoint.setNumberOfBuilding(numberOfBuilding);
		salePoint.setStreet(street);
		salePoint.setZipCode(zipCode);
		
//		System.out.println(name);
		
		salePointRepository.save(salePoint);
		
		String msg="Point of sale edited properly";
		model.addAttribute("information", msg);
		
		List<SalePoint> listOfSalePoints = (List<SalePoint>) salePointRepository.findAll();
		model.addAttribute("salePoints", listOfSalePoints);
		
		return "salePoint/all";
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String deleteSalePoint(@PathVariable("id") long id, ModelMap model) {
		
		SalePoint salePoint  = salePointRepository.findOne(id);
		List<User> salePointUsers = salePoint.getUsers();
		for(User u : salePointUsers) {
			u.getSalePoints().remove(salePointRepository.findOne(id));
		}
		salePointUsers.clear();
		
		salePointRepository.delete(salePoint);
		
		String msg="Point of sale deleted properly";
		model.addAttribute("information", msg);
		
		List<SalePoint> listOfSalePoints = (List<SalePoint>) salePointRepository.findAll();
		model.addAttribute("salePoints", listOfSalePoints);
		
		return "salePoint/all";
	}
	
	@RequestMapping(value="/addUsers/{id}", method = RequestMethod.GET)
	public String addPermissionForm(@PathVariable("id") long id, ModelMap model) {
		SalePoint salePoint = salePointRepository.findOne(id);
		model.addAttribute("salePoint", salePoint);
		
		List<User> listOfUsers = new ArrayList<>();
		List<User> users = (List<User>) userRepository.findAll();
		for(User u:users) {
			for(Permission perm : u.getPermissions()) {
				if(perm.getName().equals("worker")) {
					listOfUsers.add(u);
				}
			}
		}
		model.addAttribute("users", listOfUsers);
		
		return "salePoint/addUsers";
	}
	
	@RequestMapping(value="/addUsers/{id}", method = RequestMethod.POST)
	public String addPermissionToUser(@PathVariable("id") long id,
									  @RequestParam("users") long[] usersId,
									  ModelMap model) {
		SalePoint salePoint = salePointRepository.findOne(id);
		List<User> salePointUsers = salePoint.getUsers();
		for(User u : salePointUsers) {
			u.getSalePoints().remove(salePointRepository.findOne(id));
		}
		salePointUsers.clear();
		
		for(long userId : usersId) {
			salePointUsers.add(userRepository.findOne(userId));
		}
		
		salePointRepository.save(salePoint);
		
		String msg="Workers added properly";
		model.addAttribute("information", msg);
		
		List<SalePoint> listOfSalePoints = (List<SalePoint>) salePointRepository.findAll();
		model.addAttribute("salePoints", listOfSalePoints);
		return "salePoint/all";
	}
}
