package spring.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import spring.database.entity.Permission;
import spring.database.entity.User;
import spring.database.repository.PermissionRepository;
import spring.database.repository.UserRepository;

@Controller
public class ConstantAdminController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PermissionRepository permissionRepository;

	@RequestMapping("/getAdmin")
	public String createAdmin() {
		Optional<Permission> adminPermission = permissionRepository.findByName("administrator");

		if (!adminPermission.isPresent()) {
			Permission administratorPermission = new Permission("administrator");
			permissionRepository.save(administratorPermission);
		}

		Optional<Permission> workerPermission = permissionRepository.findByName("worker");
		if (!workerPermission.isPresent()) {
			Permission worker = new Permission("worker");
			permissionRepository.save(worker);
		}

		Optional<User> administrator = userRepository.findByLoginAndPassword("admin", "admin");
		if (!administrator.isPresent()) {
			User user = new User("Admin", "Admin", "admin", "admin");
			userRepository.save(user);
		}

		Optional<User> adminPerm = userRepository.findByLogin("admin");
		List<Permission> permissionAdmin = adminPerm.get().getPermissions();
		boolean isThereAdmin = false;
		for (Permission perm : permissionAdmin) {
//			System.out.println(perm.getName());
			if ("administrator".equals(perm.getName())) {
				isThereAdmin = true;
				break;
			}
		}
		if (isThereAdmin == false) {
//			System.out.println("Brak uprawnienia administratora");
//			System.out.println(permissionRepository.findByName("administrator").get().getName());
			User admin = userRepository.findByLoginAndPassword("admin", "admin").get();
//			System.out.println(admin.getName());
			admin.getPermissions().add(permissionRepository.findByName("administrator").get());
			userRepository.save(admin);
		}

		return "index";
	}
}
