package spring.interceptor;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import spring.database.entity.Permission;
import spring.database.repository.PermissionRepository;

public class AdminsPermissionInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		List<Permission> permissions = (List<Permission>) request.getSession()
				.getAttribute("permissionsOfUser");
		if (permissions != null && !permissions.isEmpty()) {
			for (Permission perm : permissions) {
				if ("administrator".equals(perm.getName())) {
					return super.preHandle(request, response, handler);
				}
			}
		} else {
			request.setAttribute("error", "You do not have permit to do that");
			request.getRequestDispatcher("/error/404").forward(request, response);
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
}
