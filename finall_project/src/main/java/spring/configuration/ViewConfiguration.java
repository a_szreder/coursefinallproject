package spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import spring.interceptor.EncodingIntercepter;
import spring.interceptor.AdminsPermissionInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "spring")
public class ViewConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public ViewResolver vieResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new EncodingIntercepter());
		registry.addInterceptor(new AdminsPermissionInterceptor()).addPathPatterns("/user/**")
				.addPathPatterns("/permission/**").addPathPatterns("/salePoint/**");
	}
}
