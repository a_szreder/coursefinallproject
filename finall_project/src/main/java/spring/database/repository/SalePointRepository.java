package spring.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import spring.database.entity.SalePoint;

@Repository
public interface SalePointRepository extends CrudRepository<SalePoint, Long>{

}
