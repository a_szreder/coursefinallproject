package spring.database.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity(name="permission")
public class Permission implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name = "permission_id")
	private long id;
	private String name;
	
	@ManyToMany(mappedBy="permissions")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<User> users = new ArrayList<>();
	
	public Permission() {
	}

	public Permission(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	@PreRemove
	public void removePermissionFromUsers() {
		for(User u : users) {
			u.getPermissions().remove(this);
		}
	}
}
