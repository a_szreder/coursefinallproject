package spring.database.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class SalePoint implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name = "salePoint_id")
	private long id;
	
	private String name;
	private String city;
	private String zipCode;
	private String street;
	private int numberOfBuilding;
	private int apartmentNumber;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="salePoint_user", 
		joinColumns= {@JoinColumn(name = "salePoint_id")},
		inverseJoinColumns= {@JoinColumn(name="user_id")})
	private List<User> users = new ArrayList<>();
	
	public SalePoint() {
	}

	public SalePoint(String name, String city, String zipCode, String street, int numberOfBuilding,
			int apartmentNumber) {
		this.name = name;
		this.city = city;
		this.zipCode = zipCode;
		this.street = street;
		this.numberOfBuilding = numberOfBuilding;
		this.apartmentNumber = apartmentNumber;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumberOfBuilding() {
		return numberOfBuilding;
	}

	public void setNumberOfBuilding(int numberOfBuilding) {
		this.numberOfBuilding = numberOfBuilding;
	}

	public int getApartmentNumber() {
		return apartmentNumber;
	}

	public void setApartmentNumber(int apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}
