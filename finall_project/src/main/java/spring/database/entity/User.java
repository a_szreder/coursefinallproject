package spring.database.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity(name="no_user")
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name = "user_id")
	private long id;
	
	private String name;
	private String surname;
	
	@Column(unique=true)
	private String login;
	private String password;
	private LocalDate assumptionDate;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="user_permission", 
		joinColumns= {@JoinColumn(name = "user_id")},
		inverseJoinColumns= {@JoinColumn(name="permission_id")})
	private List<Permission> permissions = new ArrayList<>();
	
	@ManyToMany(mappedBy="users")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<SalePoint> salePoints = new ArrayList<>();
	
	public User() {
	}

	public User(String name, String surname, String login, String password) {
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.password = password;
		assumptionDate = LocalDate.now();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDate getAssumptionDate() {
		return assumptionDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAssumptionDate(LocalDate assumptionDate) {
		this.assumptionDate = assumptionDate;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public List<SalePoint> getSalePoints() {
		return salePoints;
	}

	public void setSalePoints(List<SalePoint> salePoints) {
		this.salePoints = salePoints;
	}
	
	@PreRemove
	public void removeSalePointFromUser() {
		for(SalePoint salePoint : salePoints) {
			salePoint.getUsers().remove(this);
		}
	}
}
