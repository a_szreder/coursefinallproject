<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<div class="well row">
	<div class="col-sm-4 col-md-4">
		<b><p>Informations:</p></b>
			<img class="img-responsive" alt="sth" src="https://d30y9cdsu7xlg0.cloudfront.net/png/15724-200.png">
	</div>
	<div class="col-sm-8 col-md-8">

		<h1>
			<b>${user.name}</b> <b>${user.surname}</b>
		</h1>

		<p>
			Login: <b>${user.login}</b>
		</p>
		<p>
			Assumption date: <b>${user.assumptionDate}</b>
		</p>
		
		<c:if test="${user.permissions ne null}">
			<b><p>Permissions:</p></b>
				<ul>
					<c:forEach items="${user.permissions}" var="permission">
						<li>${permission.name}</li>
					</c:forEach>
				</ul>
		</c:if>
		
		<c:if test="${user.salePoints ne null}">
			<b><p>Works in:</p></b>
				<ul>
					<c:forEach items="${user.salePoints}" var="salePoint">
						<li>${salePoint.name}</li>
					</c:forEach>
				</ul>
		</c:if>
		
	</div>
</div>

<c:import url="../footer.jsp"/>