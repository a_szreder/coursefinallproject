<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Surname</th>
			<th>Assumption date</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><a href='<c:url value="/user/show/${user.id}"/>'>${user.name}</a></td>
				<td>${user.surname}</td>
				<td>${user.assumptionDate}</td>
				<td><a href='<c:url value="/user/edit/${user.id}"/>'><button
							class="btn btn-warning">Edit</button></a> <a
					href='<c:url value="/user/addPermission/${user.id}"/>'><button
							class="btn btn-info">Add permission</button></a><a
					href='<c:url value="/user/delete/${user.id}"/>'><button
							class="btn btn-danger">Delete</button></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="../footer.jsp"/>