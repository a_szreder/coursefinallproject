<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="PL">
<head>
<meta http-equiv=“Content-Type” content=“text/html; charset=utf-8">
<meta charset="UTF-8">
<title>Welcome to Casheer</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="<c:url value="/"/>" class="navbar-brand">Home Page</a>
				</div>
				<c:if test="${sessionScope.loggedIn}">
					<ul class="nav navbar-nav">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Point of sale <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/salePoint/all"/>'><span
										class="glyphicon glyphicon-th-list"></span> Show all</a></li>
								<li><a href='<c:url value="/salePoint/add"/>'><span
										class="glyphicon glyphicon-plus"></span> Add</a></li>
								<li><a href='<c:url value="/product/all"/>'><span
										class="glyphicon glyphicon-th-list"></span> Show all products</a></li>
								<li><a href='<c:url value="/product/add"/>'><span
										class="glyphicon glyphicon-plus"></span> Add new product</a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Store <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/store/all"/>'><span
										class="glyphicon glyphicon-th-list"></span> Show all</a></li>
								<li><a href='<c:url value="#"/>'><span
										class="glyphicon glyphicon-plus"></span> Add</a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Users <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="/user/all"/>'><span
										class="glyphicon glyphicon-th-list"></span> Show all</a></li>
								<li><a href='<c:url value="/user/add"/>'><span
										class="glyphicon glyphicon-plus"></span> Add</a></li>
								<li><a href='<c:url value="/permission/add"/>'><span
										class="glyphicon glyphicon-plus"></span> Add new permission</a></li>
								<li><a href='<c:url value="/permission/all"/>'><span
										class="glyphicon glyphicon-plus"></span> Show all permissions</a></li>
						</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#">Statistics <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='<c:url value="#"/>'><span
										class="glyphicon glyphicon-th-list"></span> Sell</a></li>
								<li><a href='<c:url value="#"/>'><span
										class="glyphicon glyphicon-plus"></span> Productivity</a></li>
							</ul></li>
					</ul>
				</c:if>
				<ul class="nav navbar-nav navbar-right">
					<c:if test="${sessionScope.loggedIn ne true}">
						<li><a href='<c:url value="/getAdmin"/>'><span
								class="glyphicon glyphicon-user"></span></a></li>
						<li><a href='<c:url value="/login/in"/>'><span
								class="glyphicon glyphicon-log-in"></span> Login</a></li>
					</c:if>
					<c:if test="${sessionScope.loggedIn}">
						<li><a href='<c:url value="/login/out"/>'><span
								class="glyphicon glyphicon-log-out"></span> Logout</a></li>
					</c:if>
				</ul>
			</div>
		</nav>