<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<form class="form-horizontal" action="" method="POST">
	<div class="form-group">
		<label class="control-label col-sm-3">Name:</label>
		<div class="col-sm-9">
			<input type="text" name="name" class="form-control"
				value="${product.name}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3">Price:</label>
		<div class="col-sm-9">
			<input type="text" name="price" class="form-control"
				value="${product.price}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button class="btn btn-primary">Save</button>
		</div>
	</div>
</form>

<c:import url="../footer.jsp"/>