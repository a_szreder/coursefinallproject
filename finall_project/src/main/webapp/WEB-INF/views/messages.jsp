<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${error ne null}">
	<div class="alert alert-danger">
		<p>${error}</p>
	</div>
</c:if>

<c:if test="${information ne null}">
	<div class="alert alert-info">
		<p>${information}</p>
	</div>
</c:if>

<c:if test="${success ne null}">
	<div class="alert alert-success">
		<p>${success}</p>
	</div>
</c:if>
