<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="header.jsp"/>
<c:import url="messages.jsp"/>

<form class="form-horizontal" action="" method="POST">
	<div class="form-group">
		<label class="control-label col-sm-3">Login:</label>
		<div class="col-sm-9">
			<input type="text" name="login" class="form-control"
				value="${user.login}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3">Password:</label>
		<div class="col-sm-9">
			<input type="password" name="password" class="form-control"
				value="${user.password}">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button class="btn btn-primary">Login</button>
		</div>
	</div>
</form>

<c:import url="footer.jsp"/>