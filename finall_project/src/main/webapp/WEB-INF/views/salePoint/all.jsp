<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Number of workers</th>
			<th>Store</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${salePoints}" var="salePoint">
			<tr>
				<td><a href='<c:url value="/salePoint/show/${salePoint.id}"/>'>${salePoint.name}</a></td>
				<td><c:out value="${fn:length(salePoint.users)}"/></td>
				<td><a href='<c:url value="#"/>'><button class="btn btn-basic">All products</button></a></td>
				<td><a href='<c:url value="/salePoint/edit/${salePoint.id}"/>'><button
							class="btn btn-warning">Edit</button></a><a
					href='<c:url value="/salePoint/addUsers/${salePoint.id}"/>'><button
							class="btn btn-info">Add workers</button></a><a
					href='<c:url value="/salePoint/delete/${salePoint.id}"/>'><button
							class="btn btn-danger">Delete</button></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="../footer.jsp"/>