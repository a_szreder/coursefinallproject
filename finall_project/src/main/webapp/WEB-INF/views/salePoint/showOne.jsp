<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<div class="well row">
	<div class="col-sm-4 col-md-4">
		<b><p>Informations:</p></b>
			<img class="img-responsive" alt="sth" src="http://www.iconarchive.com/download/i62141/designcontest/ecommerce-business/store.ico">
	</div>
	<div class="col-sm-8 col-md-8">

		<h1>
			<b>${salePoint.name}</b>
		</h1>

		<p>
			Address: 
			</p>
			<p>
			<b>${salePoint.street} ${salePoint.numberOfBuilding }/${salePoint.apartmentNumber } 
			${salePoint.city} ${salePoint.zipCode}</b>
		</p>
		
		
		<c:if test="${salePoint.users ne null}">
			<b><p>Workers:</p></b>
				<ul>
					<c:forEach items="${salePoint.users}" var="user">
						<li>${user.name} ${user.surname}</li>
					</c:forEach>
				</ul>
		</c:if> 
		
		
	</div>
</div>

<c:import url="../footer.jsp"/>