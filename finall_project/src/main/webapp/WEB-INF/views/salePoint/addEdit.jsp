<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="../header.jsp" />
<c:import url="../messages.jsp" />

<form class="form-horizontal" action="" method="POST">
	<div class="form-group">
		<label class="control-label col-sm-3">Name:</label>
		<div class="col-sm-9">
			<input type="text" name="name" class="form-control"
				value="${salePoint.name}">
		</div>
	</div>
	<div class="well">
		<div class="row">
			<label class="control-label col-sm-2">Address:</label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">Street:</label>
			<div class="col-sm-9">
				<input type="text" name="street" class="form-control"
					value="${salePoint.street}">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3">Building number:</label>
			<div class="col-sm-9">
				<input type="text" name="numberOfBuilding" class="form-control"
					value="${salePoint.numberOfBuilding}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">Apartment number:</label>
			<div class="col-sm-9">
				<input type="text" name="apartmentNumber" class="form-control"
					value="${salePoint.apartmentNumber}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">Zip - code:</label>
			<div class="col-sm-9">
				<input type="text" name="zipCode" class="form-control"
					value="${salePoint.zipCode}">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">City:</label>
			<div class="col-sm-9">
				<input type="text" name="city" class="form-control"
					value="${salePoint.city}">
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button class="btn btn-primary">Save</button>
		</div>
	</div>

</form>

<c:import url="../footer.jsp" />