<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${store.productQuantity}" var="productQuantity">
			<tr>
				<td>${productQuantity.product.name}</a></td>
				<td>${productQuantity.product.price}</td>
				<td>${productQuantity.quantity}</td>
				<td><a href='<c:url value="#"/>'><button
							class="btn btn-info">Add</button></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="../footer.jsp"/>