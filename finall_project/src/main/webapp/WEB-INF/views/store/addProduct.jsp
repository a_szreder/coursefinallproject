<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="../header.jsp" />
<c:import url="../messages.jsp" />

<c:choose>
	<c:when test="${fn:length(products)>0}">
		<form class="form-horizontal" action="" method="POST">
			<div class="form-group">
				<label class="control-label col-sm-3" for="sel1">Select product:</label>
				<div class="col-sm-offset-3 col-sm-9">
				 <select class="form-control" id="sel1" name="product">
					<c:forEach items="${products}" var="product">
						<option value="${product.id}">${product.name}</option>
					</c:forEach>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3">Quantity:</label>
				<div class="col-sm-offset-3 col-sm-9">
					<input type="text" name="quantity" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button class="btn btn-primary">Save</button>
				</div>
			</div>
		</form>
	</c:when>
	<c:otherwise>
		<div class="alert alert-warning">
			<p>There's no products to add!</p>
		</div>
	</c:otherwise>
</c:choose>

<c:import url="../footer.jsp" />