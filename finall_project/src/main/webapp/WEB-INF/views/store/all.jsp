<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Point of sale</th>
			<th>Products</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${stores}" var="store">
			<tr>
				<td>${store.name}</td>
				<td><a href='<c:url value="/salePoint/show/${store.salePoint.id}"/>'>${store.salePoint.name}</a></td>
				<td><a href='<c:url value="/store/allProducts/${store.id}"/>'>
					<c:out value="${fn:length(store.productQuantity)}"/></a></td>
				<td><a href='<c:url value="/store/addOne/${store.id}"/>'><button
							class="btn btn-warning">Add product</button></a> <a
					href='<c:url value="#"/>'><button
							class="btn btn-warning">Add many products</button></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="../footer.jsp"/>