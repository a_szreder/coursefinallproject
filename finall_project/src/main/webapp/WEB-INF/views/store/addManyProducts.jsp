<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="../header.jsp" />
<c:import url="../messages.jsp" />

<c:choose>
	<c:when test="${fn:length(products)>0}">
		<form class="form-horizontal" action="" method="POST">

			<div class="form-group">
				<label for="sel1" class="control-label col-sm-3">Products
					of ${store.name}:</label>
				<c:forEach items="${products}" var="product">
					<div class="checbox col-sm-offset-3 col-sm-9">
						<label> <c:set var="checked" value="" /> <c:forEach
								items="${store.products}" var="storeProduct">
								<c:choose>
									<c:when test="${storeProduct.id eq product.id}">
										<c:set var="checked" value="checked='checked'" />
									</c:when>
								</c:choose>
							</c:forEach>
							<p>
								<input type="checkbox" name="product"
									value="${product.id}" ${checked} /> ${product.name}
							</p>
						</label>
					</div>
				</c:forEach>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button class="btn btn-primary">Save</button>
				</div>
			</div>
		</form>
	</c:when>
	<c:otherwise>
		<div class="alert alert-warning">
			<p>There's no products to add!</p>
		</div>
	</c:otherwise>
</c:choose>

<c:import url="../footer.jsp" />