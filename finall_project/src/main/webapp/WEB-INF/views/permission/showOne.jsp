<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<div class="well row">
	<div class="col-sm-4 col-md-4">
		<b><p>Informations:</p></b>
			<img class="img-responsive" alt="sth" src="https://www.devsaran.com/sites/default/files/styles/large/public/blogimages/badges_dd_console_stage2.png?itok=9iOE8EyD">
	</div>
	<div class="col-sm-8 col-md-8">

		<h1>
			<b>${permission.name}</b>
		</h1>

		<c:if test="${permission.users ne null}">
			<b><p>Users with this permission:</p></b>
			<ul>
				<c:forEach items="${permission.users}" var="user">
					<li>${user.surname} ${user.name}</li>
				</c:forEach>
			</ul>
		</c:if>
	</div>
</div>

<c:import url="../footer.jsp"/>