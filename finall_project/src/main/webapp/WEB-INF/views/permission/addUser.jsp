<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<c:choose>
<c:when test="${fn:length(users)>0}">
<form class="form-horizontal" action="" method="POST">
	<div class="form-group">
		<label for="sel1" class="control-label col-sm-3">Users of ${permission.name} :</label>
		<c:forEach items="${users}" var="user">
			<div class="checbox col-sm-offset-3 col-sm-9">
				<label>
					<c:set var="checked" value=""/>
					<c:forEach items="${permission.users}" var="permissionUser">
						<c:choose>
							<c:when test="${permissionUser.id eq user.id}">
								<c:set var="checked" value="checked='checked'"/>
							</c:when>
						</c:choose>
					</c:forEach>
					<p>
						<input type="checkbox" name="users" value="${user.id}" ${checked} /> ${user.name} ${user.surname}
					</p>
				</label>
			</div>
		</c:forEach>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button class="btn btn-primary">Save</button>
		</div>
	</div>
</form>
</c:when>
<c:otherwise>
		<div class="alert alert-warning">
			<p>There's no users to add!</p>
		</div>
	</c:otherwise>
</c:choose>

<c:import url="../footer.jsp"/>