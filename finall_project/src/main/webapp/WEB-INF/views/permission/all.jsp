<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="../header.jsp"/>
<c:import url="../messages.jsp"/>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Number of people with this permission</th>
			<th>Options</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${permissions}" var="permission">
			<tr>
				<td><a href='<c:url value="/permission/show/${permission.id}"/>'>${permission.name}</a></td>
				<td><c:out value="${fn:length(permission.users)}"/></td>
				<td><a href='<c:url value="/permission/edit/${permission.id}"/>'><button
							class="btn btn-warning">Edit</button></a><a
					href='<c:url value="/permission/delete/${permission.id}"/>'><button
							class="btn btn-danger">Delete</button></a></td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<c:import url="../footer.jsp"/>